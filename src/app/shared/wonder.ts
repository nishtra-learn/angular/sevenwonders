import { ImageModel } from './imageModel';

export class Wonder {
    name: string;
    description: string;
    image: ImageModel;
}
