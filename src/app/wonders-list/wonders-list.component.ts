import { Component, OnInit } from '@angular/core';
import { Wonder } from '../shared/wonder';
import { WondersDataProviderService } from '../shared/wonders-data-provider.service';

@Component({
  selector: 'app-wonders-list',
  templateUrl: './wonders-list.component.html',
  styleUrls: ['./wonders-list.component.css']
})
export class WondersListComponent implements OnInit {
  wonders: Wonder[] = [];

  constructor(private wondersData: WondersDataProviderService) { 
    this.wonders = [...wondersData.wonders];
  }

  ngOnInit(): void {
  }

}
